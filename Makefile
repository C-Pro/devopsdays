.PHONY: clean test docker all

all: bin/timeserver docker

bin/timeserver: index.go
	CGO_ENABLED=0 go build -o bin/timeserver

index.go:
	go generate

clean:
	rm -f index.go
	rm -f bin/timeserver

test:
	go test -v .

docker:
	docker build -t registry.gitlab.com/c-pro/devopsdays:latest .
