//Generated file - DO NOT EDIT!
package main

var index = []byte(`<html>

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>CI Demo Page</title>
    <link href="https://fonts.googleapis.com/css?family=Press+Start+2P" rel="stylesheet">
    <style type="text/css">
        .time {
            font-size: 72pt;
            color: #000000;
            font-family: 'Press Start 2P', cursive;
        }
    </style>
</head>
<script type="text/javascript">

    function getBaseURL() {
        var locURL = window.location.href
        var baseURL = "http://localhost:8080"
        if (locURL.startsWith("http://") ||
            locURL.startsWith("https://")) {
            baseURL = locURL.substr(0, locURL.lastIndexOf("/"));
        }
        return baseURL;
    }

    function getTime() {
        fetch(getBaseURL() + "/time")
            .then(function (response) {
                return response.text();
            })
            .then(function (text) {
                document.getElementById("time").innerHTML = text;
            })
            .catch(console.log);
    }


    function start() {
        setInterval("getTime()", 500);
    }

</script>

<body onload="start()">
    <table border="0" width="100%" height="100%">
        <tr height="100%">
            <td width="100%" valign="middle">
                <div align="center" valign="center" class="time" id="time"></div>
            </td>
        </tr>
    </table>
</body>

</html>
`)
