//go:generate go run ui/generate.go ui/index.html index.go
package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

const timeFormat = "2006-01-02 15:04:05"

func logRequest(r *http.Request) {
	log.Printf("%s %s: %s %s\n", r.RemoteAddr, r.Referer(), r.Method, r.RequestURI)
}

// IndexHandler writes current time in UTC to response
func IndexHandler(w http.ResponseWriter, r *http.Request) {
	logRequest(r)
	w.Header().Add("Content-Type", "text/html; charset=utf-8")
	if _, err := w.Write(index); err != nil {
		log.Printf("Failed to write answer: %v", err)
	}
}

// TimeHandler writes current time in UTC to response
func TimeHandler(w http.ResponseWriter, r *http.Request) {
	logRequest(r)
	w.Header().Add("Access-Control-Allow-Origin", "*")
	fmt.Fprint(w, time.Now().UTC().Format(timeFormat))
}

func main() {
	http.HandleFunc("/", IndexHandler)
	http.HandleFunc("/time", TimeHandler)
	if err := http.ListenAndServe("0.0.0.0:8080", nil); err != nil {
		panic(err)
	}
}
