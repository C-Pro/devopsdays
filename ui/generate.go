package main

import (
	"io/ioutil"
	"os"
)

var (
	header = []byte(`//Generated file - DO NOT EDIT!
package main

var index = []byte(` + "`")
	footer = []byte("`)\n")
)

func main() {
	b, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		panic(err)
	}
	b = append(header, b...)
	b = append(b, footer...)
	err = ioutil.WriteFile(os.Args[2], b, 0644)
	if err != nil {
		panic(err)
	}
}
