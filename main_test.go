package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestTimeHandler(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(TimeHandler))
	client := http.Client{}
	req, err := http.NewRequest("GET", srv.URL+"/time", nil)
	if err != nil {
		t.Fatalf("Unexpected error in NewRequest: %v", err)
	}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Unexpected error in client.Do: %v", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		t.Errorf("Expected code 200, got %d", resp.StatusCode)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("Unexpected error in ReadAll: %v", err)
	}
	resTime, err := time.Parse(timeFormat, string(body))
	if err != nil {
		t.Fatalf("Unexpected error in time.Parse: %v", err)
	}
	if time.Now().UTC().Sub(resTime) > time.Second ||
		resTime.Sub(time.Now().UTC()) > time.Second {
		t.Errorf("Returned time is too far away: %v", resTime)
	}
}

func TestIndexHandler(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(IndexHandler))
	client := http.Client{}
	req, err := http.NewRequest("GET", srv.URL+"/", nil)
	if err != nil {
		t.Fatalf("Unexpected error in NewRequest: %v", err)
	}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Unexpected error in client.Do: %v", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		t.Errorf("Expected code 200, got %d", resp.StatusCode)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("Unexpected error in ReadAll: %v", err)
	}
	index, err := ioutil.ReadFile("ui/index.html")
	if err != nil {
		t.Fatalf("Unexpected error in ReadFile: %v", err)
	}
	if len(body) != len(index) {
		t.Fatalf("Got %d bytes, expected %d", len(body), len(index))
	}
	for i := 0; i < len(body); i++ {
		if body[i] != index[i] {
			t.Fatalf("Received body does not match expected. Byte number %d", i)
		}
	}
}
