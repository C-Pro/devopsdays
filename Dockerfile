FROM scratch
ADD bin/timeserver /
EXPOSE 8080
CMD ["/timeserver"]
